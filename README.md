# Team A3

Team A3 ITT2 datacenter project

Jacob B. Koch

Máté Burján

Allan Nielsen

Bogdan Robert

Can Gabriel

Andrei Romar

Mark Christiansen

ITT + OME google docs: https://docs.google.com/document/d/1liAEKhnZU3HPJ4CXYUXewAJRsHygR80M7zV6TosrHtU/edit# 

[OME Research](./Ventilation project/OME.md)

[Scrum master/assistant plan](./Management/Scrum.md)

[Milestones](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3/-/milestones)

[Block Diagram](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3/-/blob/master/Ventilation%20project/block_diagram)

[POC Video](https://youtu.be/Ecadi1VEvB8)

[Sensors](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3-group/sensors)

[Mqtt](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3-group/mqtt)

[Business Understanding](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3/-/blob/master/Ventilation%20project/Business_understanding.pdf)
