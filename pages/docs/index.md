# Ventilation by Team A3

## Project management and Scrum

We started with GPit and Gantt as you can see here
[Management](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3/-/tree/master/Management)

We also used Scrum weekly meeting every monday to list what was beeing worked on that week.

## Organizing gitlab

We split our gitlab up into 4 parts, that you can see here.
[Gitlab](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3-group)

## System purpose and use

Our system is made to give data about the temperature and air pressure of the ventilation.
It purpose is to collect and show data so we can maximize uptime of the datacenter.
It is placed in key parts of the ventilation so it can automatically collect the data and show it on a node red dashboard.

## Block diagram

![Block diagram](img/block.jpg)<br/>

## Use case

![Use case](img/Usecase.png)<br/>

## MQTT

We have setup an Azure server with MQTT.
There we are publishing our sensor data:<br/>
![Vent.py](img/vent.png)<br/>

And from another server we are subscribing to the MQTT and inserting it into MongoDB
[mqtt-to-db.py](https://gitlab.com/21s-itt2-datacenter-students-group/team-a3/-/blob/master/Ventilation%20project/code/mqtt-to-db.py)


## Sensors

DHT11<br/>
![DHT11](img/DHT11.png)<br/>
<br/>
D6F-PH<br/>
![D6F-PH](img/D6F-PH.png)<br/>
