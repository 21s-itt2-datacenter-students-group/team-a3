import time
import random
#import D6F
#D6Fmod = D6F.D6fph()

#import DHT as DHT
#DHTPin = 11     #define the pin of DHT11


import paho.mqtt.client as mqtt 
mqttBroker ="teama3mqtt.westeurope.cloudapp.azure.com" 

client = mqtt.Client()
client.connect(mqttBroker, 1883, 60) 

temp=0
hum=0
pressure=0
while True:
#	pressure=D6Fmod.read_pressure()
#	dht = DHT.DHT(DHTPin)
#	chk = dht.readDHT11()
#	if (chk is dht.DHTLIB_OK):
#		temp=dht.temperature
#		hum=dht.humidity	
	hum=12
	temp=random.randint(15,25)
	pressure=random.randint(-15,30)
	client.publish("Temperature", round(temp, 2))
	client.publish("Pressure", round(pressure, 2))
	print("Humidity: %.2f, \t Temperature: %.2f,  \t Pressure: %.2f \n"%(hum,temp,pressure))
	time.sleep(1)