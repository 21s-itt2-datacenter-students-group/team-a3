import paho.mqtt.client as mqtt 
from random import randrange
import time

mqttBroker ="teama3mqtt.westeurope.cloudapp.azure.com" 

client = mqtt.Client("temp")
client.connect(mqttBroker, 1883) 

while True:
    client.publish("temp", 23)
    print("Just published temperature to topic temp")
    time.sleep(1)
